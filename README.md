Custom reticles for GZDoom mod Hideous Destructor.

Contains:
  • Small T (rret 8);
  • Corner (rret 9);
  • Mirrored corner (rret 10);
  • Half-plus half-transparent (rret 11);
  • Mirrored half-plus half-transparent (rret 12);
  • Over-engineered crosshair (rret 13);
  • Upside-down OK symbol (rret 7975);
  • Immature symbol (rret 8468);
  • Immature crosshair (rret 8080);
  • Helmet (rret 727677);
  • Astronaut (rret 838583);
  • Troll (rret 848276).